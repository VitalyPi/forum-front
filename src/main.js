// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VuePagination from 'vue-bs-pagination'

Vue.config.productionTip = false;
Vue.use(require('vue-moment'));
Vue.component('v-pagination', VuePagination);
axios.defaults.baseURL = 'http://localhost:7222';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
